
import solve_rp
from gmpy2 import mpfr

#---------- Lifting Assignment to FP model ------------#


# Search_model: float(mpfr) float(mpfr) --> dict

# GIVEN: 1. Prop-exp 
#   	 2. A floating point variable 'x' whose value is to be lifted
#		 3. Another floating point variable denoting the bound beyond which
#			the value of the variable x should not be modified.

# RETURNS: delta_dict{}, consisting of the value by which the original value of 'x' has been lifted


# CONSTANTS:
EPS_ORIGINAL = 0.000000001 # this should really be the minimum positive FP number
PHI_EXP = 1.1 # 1.1 seems very small
PHI_INCREMENT_EXP = 0.1
#solve_rp.LEAST_NUM




def search_model(propexp, x, delta_max):

	solve_rp.get_context()
	delta_dict = {}
	abst = mpfr(x.fp_val)
        print("#>>>>>>>>>>>>>>>The variable to be lifted is " + str(abst))
        print("#>>>>>>>>>>>>>>>The eps is " + str(mpfr(EPS_ORIGINAL)))
	sat_assign = exponential_search(propexp, x, mpfr(abst), mpfr(abst), mpfr(EPS_ORIGINAL), mpfr(PHI_EXP), 0)
	if sat_assign != -1:    
            delta = mpfr(sat_assign - abst) #if sat_assign > abst else abst - sat_assign
            print("#>>>>>>>>>>>>>>>>>>>>The delta is: " + str(delta))
            print("#>>>>>>>>>>>>>>>>>>>>The original var is: " + str(abst))
	    delta_dict["delta_"+x.name] = delta
	    return delta_dict
	else:
	    print("#>>>>>>>>>>>>>>>Solution Unknown")
	    return {}




def exponential_search(propexp, x, xl, xh, eps, phi, recur_count):

    # Check on the number of recursions; returns -1 when a certain number of
    # recursions are done and no solution is found
    recur_count += 1
    if recur_count > 500:
        print("Time-out")
        return -1

    # Increments phi after every 5 recursions
    if recur_count % 5 == 0:
        eps = eps * phi
            
    #    phi = phi + PHI_INCREMENT_EXP   

    # Updating epsilon
    # eps = eps * phi
    xl = mpfr(xl - eps)
    xh = mpfr(xh + eps)

    x.fp_val = xl
    #print("#>>>>>>>>>>>>>>>The lower assignment is : " + str(x.fp_val))
    lower_sol = evaluator_function(propexp, x, -eps)

    if lower_sol != -1:
    	return lower_sol
    else:
    	x.fp_val = xh
    	higher_sol = evaluator_function(propexp, x, eps)

    	if higher_sol != -1:
    		return higher_sol
    	else:
    		return exponential_search(propexp, x, xl, xh, eps, phi, recur_count)
    
    #x.fp_val = xh
    #print("#>>>>>>>>>>>>>>>The higher assignment is : " + str(x.fp_val))
    #higher_sol = evaluator_function(propexp, x, eps)

    
    #if lower_sol == -1 and higher_sol == -1:
     #   return exponential_search(propexp, x, xl, xh, eps, phi, recur_count)
    # Else if xl or xh satisfy prop-exp, return that value
    #else:
     #   return lower_sol if lower_sol != -1 else higher_sol


def evaluator_function(propexp, x, eps):

    # Evaluates prop-exp with x; returns boolean
    #print("#>>>>>>>>>>>>>>>Inside exponential_search_help")
    is_sat = propexp.evaluate_fp_update(x)

    # If prop-exp is UNSAT, return -1
    if is_sat == False:
        return -1
    # If prop-exp is SAT, proceed with binary_search()
    # The function takes last unsatisfying value (unsat_val) and
    # first satisfying value (sat_val)
    else:
        unsat_val = mpfr(x.fp_val - eps)
        sat_val = mpfr(x.fp_val)
        print("#>>>>>>>>>>>>>>>The interval is " + str(unsat_val) + " to " + str(sat_val))
        print("#>>>>>>>>>>>>>>>Switching to Binary Search ---")
        return binary_search(propexp, x, unsat_val, sat_val)


# --------------PART 2-------------#


def binary_search(propexp, x, unsat_val, sat_val):

    # Evaluates prop-exp with x; returns boolean
    # evaluate.fp() - function from Jaideep's code
    #print("Inside binary_search")
    x.fp_val = (unsat_val + sat_val) / mpfr('2')
    #print("unsat_val: " + str(unsat_val) + " and sat_val: " + str(sat_val))
    #print("value of assignment is: " + str(x.fp_val))
    is_sat = propexp.evaluate_fp_update(x)


    if x.fp_val == unsat_val or x.fp_val == sat_val:
    	print("#>>>>>>>>>>>>>>>The final value is: " + str(sat_val))
    	return sat_val

    if is_sat == False:
    	return binary_search(propexp, x, x.fp_val, sat_val)
    else:
        return binary_search(propexp, x, unsat_val, x.fp_val)







