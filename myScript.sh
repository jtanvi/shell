echo "Below are the results for the benchmarks:" > myResults.txt

count=23

for example in `cat example_list.txt`
do
	echo "\n\n" >> myResults.txt
	space=". "
	echo $count$space$example >> myResults.txt

	pre="examples/new_polynomials/"
	post=".fml"
	name=$pre$example$post
	echo $pre

	#increment benchmark number
	count=`expr $count + 1`

	#example execution:
	python solve_rp.py $name examples/single-prec-modified2.cfg lfp > myTerminal.txt
	

	set $`grep "THE SATISFYING FP ASSIGNMENT" myTerminal.txt`
	exit_stat0=`echo $?`
	if [ "$exit_stat0" -eq "0" ]
	then
		awk '/THE SATISFYING FP ASSIGNMENT IS:/' RS='%{15}' myTerminal.txt >> myResults.txt
	else
		echo "No assignment found.\n" >> myResults
	fi


	#No of iterations:
	set $`grep "iteration" summary.txt | tail -1`
		iter_count=`echo $2`
		echo "No. of iterations: "$iter_count >> myResults.txt



	#get other details:--
	grep "&&" myTerminal.txt
	exit_stat1=`echo $?`
	echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"$exit_stat1
	if [ "$exit_stat1" -eq "1" ]
	then

		# Was lifting used?
		echo "Using Numeric Lifting?: YES" >> myResults.txt

		# Time taken:
		set $`grep "time" summary.txt | tail -1`
		echo "Time taken: "$2 >> myResults.txt

		# Original value of the variable that was lifted:
		set $`grep "original var" myTerminal.txt | tail -1`
		echo "Value of variable before lifting was: "$5 >> myResults.txt

		#Final value of the modified variable:
		set $`grep "final value" myTerminal.txt | tail -1`
		echo "Value of variable after lifting is: "$5 >> myResults.txt
		
		#Delta:
		set $`grep "delta" myTerminal.txt | tail -1`
		echo "Delta is: "$4 >> myResults.txt

	else

		#No of iterations:
		#iter_count=`echo $5`
		#echo "No. of iterations: "$iter_count >> myResults.txt

		# Was lifting used?
		echo "Using Numeric Lifting?: NO" >> myResults.txt

	fi


	#get final delta, original assignment and final assignment:--

	
done